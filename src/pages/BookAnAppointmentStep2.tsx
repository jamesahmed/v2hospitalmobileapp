import {
  IonButton,
  IonButtons,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCol,
  IonContent,
  IonGrid,
  IonHeader,
  IonImg,
  IonLabel,
  IonModal,
  IonPage,
  IonRadio,
  IonRadioGroup,
  IonRow,
  IonText,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import React, { useCallback, useState } from "react";
import Footer from "./Footer";
import Header from "./Header";
import "./BookAnAppointmentStep2.css";
import Doctor from "../images/Doctor-01.png";
import "@natscale/react-calendar/dist/main.css";
import { Calendar } from "@natscale/react-calendar";

const BookAnAppointmentStep2: React.FC = () => {
  var times: any[] = [
    "7:00 AM",
    "7:30 AM",
    "8:00 AM",
    "8:30 AM",
    "9:00 AM",
    "9:30 AM",
    "10:00 AM",
    "10:30 AM",
    "11:00 AM",
    "11:30 AM",
    "12:00 PM",
    "12:30 PM",
    "1:00 PM",
    "1:30 PM",
    "2:00 PM",
    "2:30 PM",
    "3:00 PM",
  ];

  var days: any[] = ["SU", "MO", "TU", "WE", "TH", "FR", "SA"];

  const [value, setValue] = useState();
  const [showModal, setShowModal] = useState(false);
  const [selectedIndex, setselectedIndex] = useState(0);
  const [disableTimeSlot, setenableTimeSlot] = useState(true);
  const [disableConfirmAppointment, setdisableConfirmAppointment] =
    useState(true);

  const onChange = useCallback(
    (value) => {
      setValue(value);
      setenableTimeSlot(false);
    },
    [setValue]
  );

  const onClose = () => {
    setShowModal(false);
    setdisableConfirmAppointment(false);
  };

  return (
    <IonPage>
      <Header title={"Book Appointment Step 2"} defaultRoute={"/Main"} />
      <IonContent>
        <IonGrid class="b2-grid">
          <IonRadioGroup value="online">
            <IonRow class="b2-rowradioStyle">
              <IonCol>
                <IonRadio value="online" class="b2-radioStyle" />
                <IonLabel>
                  <b>ONLINE CONSULTATION</b>
                </IonLabel>
              </IonCol>
              <IonCol>
                <IonRadio value="physical" class="b2-radioStyle" />
                <IonLabel>
                  <b>PHYSICAL APPOINTMENT</b>
                </IonLabel>
              </IonCol>
            </IonRow>
          </IonRadioGroup>
          <IonRow class="b2-row">
            <IonCol size="2">
              <IonImg src={Doctor}></IonImg>
            </IonCol>
            <IonCol size="10" class="b2-textCol">
              <IonText class="b2-text1">
                <b>DR. AHMED GAAFAR ZABADY</b>
              </IonText>
              <IonText class="b2-text2">INTERNAL MEDICINE</IonText>
              <IonText class="b2-text3">INTERNAL MEDICINE CONSULTANT</IonText>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol class="b2-cardCol">
              <IonCard>
                <IonCardHeader class="b2-cardheader">
                  <b>AL HILAL WEST, DOHA, QATAR</b>
                </IonCardHeader>
                <IonCardContent>
                  <IonLabel class="b2-label1">
                    <b>AVAILABLE ON DAYS</b>
                  </IonLabel>
                  <br></br>
                  <div className="b2-div">
                    {days.map((d) => (
                      <div className="b2-span">{d}</div>
                    ))}
                  </div>
                </IonCardContent>
              </IonCard>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <div>
                {/* https://reactjsexample.com/a-lightweight-and-feature-rich-calendar-component-for-react/ */}
                <Calendar value={value} onChange={onChange} />
              </div>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <IonModal isOpen={showModal} cssClass="my-custom-class">
                <IonHeader translucent>
                  <IonToolbar>
                    <IonTitle>CHOOSE TIME SLOT</IonTitle>
                    <IonButtons slot="end">
                      <IonButton onClick={onClose}>Close</IonButton>
                    </IonButtons>
                  </IonToolbar>
                </IonHeader>
                <IonContent fullscreen>
                  <IonGrid>
                    <IonRow>
                      {times.map((item, index) => (
                        <IonCol size="6">
                          <IonButton
                            expand="full"
                            onClick={() => setselectedIndex(index)}
                            color={
                              selectedIndex === index ? "light" : "primary"
                            }
                          >
                            {item}
                          </IonButton>
                        </IonCol>
                      ))}
                    </IonRow>
                  </IonGrid>
                </IonContent>
              </IonModal>
              <IonButton
                onClick={() => setShowModal(true)}
                disabled={disableTimeSlot}
                size="small"
              >
                CHOOSE TIME SLOT
              </IonButton>{" "}
              <IonButton
                routerLink="/BookAnAppointmentStep3"
                disabled={disableConfirmAppointment}
                size="small"
              >
                CONFIRM APPOINTMENT
              </IonButton>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
      <Footer />
    </IonPage>
  );
};

export default BookAnAppointmentStep2;
