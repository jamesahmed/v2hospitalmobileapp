import React, {useState} from "react";
import {
  IonBackButton,
  IonButton,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCheckbox,
  IonCol,
  IonContent,
  IonGrid,
  IonPage,
  IonRow,
  IonText,
} from "@ionic/react";
import "./Registration.css";

const RegistrationStep4: React.FC = () => {
 
  const [disableConfirmBtn, setdisableConfirmBtn] = useState(true);

  const CheckStatus = (event: any) => {
    if (event) setdisableConfirmBtn(false);
    else setdisableConfirmBtn(true);
  };

  return (
    <IonPage>
      <IonContent class="ionContent">
      <IonGrid class="headergrid">
          <IonRow>
           <IonBackButton defaultHref="RegistrationStep3" color="light" class="backbutton"/>
            <IonCol>
              <h3 className="registerLabel">
                <IonText>
                  {" "}
                  <b>REGISTRATION</b>
                </IonText>
              </h3>
            </IonCol>
          </IonRow>
          </IonGrid>
          <IonGrid class="maingrid">
          <IonRow>
            <IonCol class="secodnaryGrid">
              <IonCard class="mainCardStyle card">
                <IonCardHeader class="cardHeader">
                  <h3>
                    <b>STEP-4</b>
                  </h3>
                </IonCardHeader>
                <IonCardContent>
                  <div className="divClass">
                    <IonRow>
                      <IonCol size="2">
                        <IonCheckbox 
                        class="checkBox" 
                        color="primary"  
                        onIonChange={(e) => CheckStatus(e.detail.checked)}/>{" "}
                        {"     "}
                      </IonCol>
                      <IonCol size="10" class="textCol">
                        <IonText class="textStyle">
                          Agree to terms and conditions
                        </IonText>
                      </IonCol>
                    </IonRow>
                  </div>
                  <IonRow>
                    <IonCol>
                      <IonButton
                        expand="block"
                        routerLink="/Login"
                        class="nextBtn"
                        strong={true}
                        disabled={disableConfirmBtn}
                      >
                        SUBMIT
                      </IonButton>
                    </IonCol>
                  </IonRow>
                </IonCardContent>
              </IonCard>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default RegistrationStep4;
