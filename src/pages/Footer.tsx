import { IonFooter, IonTitle, IonToolbar } from "@ionic/react";
import React from "react";
import "./Footer.css";

const Footer: React.FC = () => {
  return (
    <IonFooter>
      <IonToolbar class="ionToolbar">
        <IonTitle class="ion-title">www.alemadihospital.com.qa</IonTitle>
      </IonToolbar>
    </IonFooter>
  );
};

export default Footer;
