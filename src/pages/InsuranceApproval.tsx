import {
  IonButton,
  IonCol,
  IonContent,
  IonGrid,
  IonPage,
  IonRow,
} from "@ionic/react";
import Footer from "./Footer";
import Header from "./Header";
import Accordion from "../components/Accordion";
import "./InsuranceApproval.css";

const InsuranceApproval: React.FC = () => {
  interface Idetails {
    name: string;
    status: string;
  }

  const arrayOfDetails: Array<Idetails> = [
    { name: "ORDER LAB", status: "APPROVED" },
    { name: "ORDER SURGERY", status: "APPROVED" },
    { name: "ORDER (MRI)", status: "APPROVED" },
  ];

  return (
    <IonPage>
      <Header title={"InsuranceApproval page"} defaultRoute={"/Main"} />
      <IonContent fullscreen className="ion-padding">
        <Accordion /*https://www.youtube.com/watch?v=AawmiNgjKR4 */
          list={arrayOfDetails}
          renderHeader={(item: Idetails) => {
            return (
              <span style={{ fontWeight: "bold", textTransform: "uppercase" }}>
                {item.name}
              </span>
            );
          }}
          renderPanel={(item: any) => {
            return (
              <IonGrid>
                <IonRow class="row">
                  <IonCol>ORDCODE: ORD00000852237</IonCol>

                  <IonCol>CODE: IMM71</IonCol>

                  <IonCol>DESC: INFLUENZA A NASAL SWAB </IonCol>

                  <IonButton class="textStatus" expand="full">
                    Approved
                  </IonButton>
                </IonRow>{" "}
                <IonRow class="row">
                  <IonCol>ORDCODE: ORD00000852237 </IonCol>

                  <IonCol>CODE: SE15 </IonCol>

                  <IonCol>DESC: STREP A NASOPHARYNGEAL SWAB </IonCol>

                  <IonButton class="textStatus" color="danger" expand="full">
                    Rejected
                  </IonButton>
                </IonRow>
              </IonGrid>
            );
          }}
        />
      </IonContent>
      <Footer />
    </IonPage>
  );
};

export default InsuranceApproval;
