import React, {useState} from "react";
import {
  IonBackButton,
  IonButton,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCol,
  IonContent,
  IonGrid,
  IonLabel,
  IonPage,
  IonRow,
  IonSelect,
  IonSelectOption,
  IonText,
  IonIcon
} from "@ionic/react";
import "./Registration.css";
import { camera } from "ionicons/icons";
import {Camera, CameraSource, CameraResultType} from '@capacitor/camera'

const RegistrationStep3: React.FC = () => {
  const [takenPhotoFront, setTakenPhotoFront] = useState<{path:string;filename:any;}>();
  const [takenPhotoBack, setTakenPhotoBack] = useState<{path:string;filename:any;}>();
  const [takenPhotoInsurance, setTakenPhotoInsurance] = useState<{path:string;filename:any;}>();

  // Upload QID Front
   const takePhotoFront = async () => {
     const photo = await Camera.getPhoto({
       resultType: CameraResultType.Uri,
       //(Prompt-Prompts the user to select either the photo album or take a photo)
       //(Camera-Take a new photo using the camera.)
       source: CameraSource.Camera, 
       allowEditing: false,
       quality:80,
     });
     if(!photo || !photo.path || !photo.webPath){
       return;
     }

    var x = photo.path.split('/');    

     setTakenPhotoFront({
       path: photo.path,
       filename: x.pop()
     })
     console.log(photo);
   }

     // Upload QID Back
   const takePhotoBack= async () => {
    const photo = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera,
      quality:80,
      width:200

    });
    if(!photo || !photo.path || !photo.webPath){
      return;
    }
    var x = photo.path.split('/');    
    setTakenPhotoBack({
      path: photo.path,
      filename: x.pop()
    })
    console.log(photo);
  }

    // Upload Insurance
  const takePhotoInsurance = async () => {
    const photo = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera,
      quality:80,
      width:200

    });
    if(!photo || !photo.path || !photo.webPath){
      return;
    }
    var x = photo.path.split('/');    
    setTakenPhotoInsurance({
      path: photo.path,
      filename: x.pop()
    })
    console.log(photo);
  }
  return (
    <IonPage>
      <IonContent class="ionContent">
      <IonGrid class="headergrid">
          <IonRow>
           <IonBackButton defaultHref="RegistrationStep2" color="light" class="backbutton"/>
            <IonCol>
              <h3 className="registerLabel">
                <IonText>
                  {" "}
                  <b>REGISTRATION</b>
                </IonText>
              </h3>
            </IonCol>
          </IonRow>
          </IonGrid>
          <IonGrid class="maingrid">
          <IonRow>
            <IonCol class="secodnaryGrid">
              <IonCard class="mainCardStyle card">
                <IonCardHeader class="cardHeader">
                  <h3>
                    <b>STEP-3</b>
                  </h3>
                </IonCardHeader>
                <IonCardContent>
                  <div className="divClass">
                    <IonRow>
                      <IonCol>
                        {" "}
                        <IonLabel class="inputlabel">Upload QID</IonLabel>
                                             
                        <IonButton fill="solid" onClick={takePhotoFront}>
                          <IonIcon icon={camera} slot="start"/>
                          <IonLabel>front</IonLabel>
                        </IonButton>
                        <div>
                        {!takenPhotoFront && <h3> No photo chosen</h3>}
                        {takenPhotoFront && <h4>{takenPhotoFront.filename}</h4>}
                        </div>
                                               
                         <IonButton fill="solid" onClick={takePhotoBack}>
                          <IonIcon icon={camera} slot="start"/>
                          <IonLabel >Back</IonLabel>
                        </IonButton>
                        <div>
                        {!takenPhotoBack && <h3> No photo chosen</h3>}
                        {takenPhotoBack && <h4>{takenPhotoBack.filename}</h4>}
                        </div>
                      </IonCol>
                    </IonRow>
                    <IonRow>
                      <IonCol>
                        {" "}
                        <IonLabel class="inputlabel">
                          Insurance Company
                        </IonLabel>
                        <IonSelect value="00" class="multiselect" className="dropdown">
                         <IonSelectOption value="01">SEVEN SERVICE                            </IonSelectOption>
                          <IonSelectOption value="02">ANTI DOPING LAB QATAR                   </IonSelectOption>
                          <IonSelectOption value="03">AETNA                                   </IonSelectOption>
                          <IonSelectOption value="04">AL KOOT                                 </IonSelectOption>
                          <IonSelectOption value="05">METLIFE(ALICO)                          </IonSelectOption>
                          <IonSelectOption value="06">ALMADALLAH                              </IonSelectOption>
                          <IonSelectOption value="07">ALLINAZ                                 </IonSelectOption>
                          <IonSelectOption value="08">AMITY HEALTH(COMPREHENSIVE NETWORK ONLY)</IonSelectOption>
                          <IonSelectOption value="09">ASPETAR                                 </IonSelectOption>
                          <IonSelectOption value="10">AXA INSURANCE (GULF) B.S.C (C)          </IonSelectOption>
                          <IonSelectOption value="11">BUPA                                    </IonSelectOption>
                          <IonSelectOption value="12">CIGNA                                   </IonSelectOption>
                          <IonSelectOption value="13">CROWNTOWNS - NOOR CARE                  </IonSelectOption>
                          <IonSelectOption value="14">CROWNTOWNS - NOOR CARE (NORTH GATE)     </IonSelectOption>
                          <IonSelectOption value="15">DAMAN UAE                               </IonSelectOption>
                          <IonSelectOption value="16">DAMAN QATAR                             </IonSelectOption>
                          <IonSelectOption value="17">ENGINEERING CONSULTANT GROUP            </IonSelectOption>
                          <IonSelectOption value="18">ENGINEERING CONSULTANT GROUP(NORTH GATE)</IonSelectOption>
                          <IonSelectOption value="19">EMBASSY OF THE STATE OF KUWAIT          </IonSelectOption>
                          <IonSelectOption value="20">GEMS CO.W.L.L                           </IonSelectOption>
                          <IonSelectOption value="21">GEO BLUE                                </IonSelectOption>
                          <IonSelectOption value="22">INTERGLOBAL                             </IonSelectOption>
                          <IonSelectOption value="23">GMC SERVICES / HENNER                   </IonSelectOption>
                          <IonSelectOption value="24">GENERAL RETIREMENT AND SOCIAL INSURANCE </IonSelectOption>
                          <IonSelectOption value="25">HEALTH 360                              </IonSelectOption>
                          <IonSelectOption value="26">INAYAH                                  </IonSelectOption>
                          <IonSelectOption value="27">ITT                                     </IonSelectOption>
                          <IonSelectOption value="28">GLOBEMED                                </IonSelectOption>
                          <IonSelectOption value="29">MINORS AFFAIRS AUTHORITY                </IonSelectOption>
                          <IonSelectOption value="30">MINORS AFFAIRS AUTHORITY ( North Gate ) </IonSelectOption>
                          <IonSelectOption value="31">Mednet Bahrain                          </IonSelectOption>
                          <IonSelectOption value="32">MEDDY                                   </IonSelectOption>
                          <IonSelectOption value="33">MINISTRY OF MUNICIPALITY AND ENVIRONMENT</IonSelectOption>
                          <IonSelectOption value="34">MOORE                                   </IonSelectOption>
                          <IonSelectOption value="35">MOORE (North Gate)                      </IonSelectOption>
                          <IonSelectOption value="36">MOBILITY SAINT HONORE (MSH) INTERNATIONA</IonSelectOption>
                          <IonSelectOption value="37">MUNTAJAT                                </IonSelectOption>
                          <IonSelectOption value="38">NAS                                     </IonSelectOption>
                          <IonSelectOption value="39">NESTLE                                  </IonSelectOption>
                          <IonSelectOption value="40">NEURON                                  </IonSelectOption>
                          <IonSelectOption value="41">NEXTCARE                                </IonSelectOption>
                          <IonSelectOption value="42">SEVEN SERVICES - NG                     </IonSelectOption>
                          <IonSelectOption value="43">NORTH BRANCH ANTI DOPING LAB QATAR      </IonSelectOption>
                          <IonSelectOption value="44">NORTH BRANCH AETNA                      </IonSelectOption>
                          <IonSelectOption value="45">NORTH BRANCH Al Koot                    </IonSelectOption>
                          <IonSelectOption value="46">NORTH BRANCH METLIFE (ALICO)            </IonSelectOption>
                          <IonSelectOption value="47">NORTH BRANCH ALMADALLAH                 </IonSelectOption>
                          <IonSelectOption value="48">NORTH BRANCH ALLIANZ                    </IonSelectOption>
                          <IonSelectOption value="49">NORTH BRANCH AMITY HEALTH (COMPREHENSIVE</IonSelectOption>
                          <IonSelectOption value="50">NORTH BRANCH ASPETAR                    </IonSelectOption>
                          <IonSelectOption value="51">NORTH BRANCH AXA INSURANCE (GULF) B.S.C </IonSelectOption>
                          <IonSelectOption value="52">NORTH BRANCH BUPA                       </IonSelectOption>
                          <IonSelectOption value="53">NORTH BRANCH CIGNA                      </IonSelectOption>
                          <IonSelectOption value="54">NORTH BRANCH DAMAN UAE                  </IonSelectOption>
                          <IonSelectOption value="55">NORTH BRANCH DAMAN QATAR                </IonSelectOption>
                          <IonSelectOption value="56">NORTH BRANCH FMC                        </IonSelectOption>
                          <IonSelectOption value="57">NORTH BRANCH GEMS CO. W.L.L             </IonSelectOption>
                          <IonSelectOption value="58">NORTH BRANCH GEO BLUE                   </IonSelectOption>
                          <IonSelectOption value="59">NORTH BRANCH INTERGLOBAL                </IonSelectOption>
                          <IonSelectOption value="60">NORTH BRANCH GMC SERVICES / HENNER      </IonSelectOption>
                          <IonSelectOption value="61">NORTH GENERAL RETIREMENT AND SOCIAL INS </IonSelectOption>
                          <IonSelectOption value="62">NORTH BRANCH HEALTH 360                 </IonSelectOption>
                          <IonSelectOption value="63">NORTH BRANCH INAYAH                     </IonSelectOption>
                          <IonSelectOption value="64">NORTH BRANCH ITT                        </IonSelectOption>
                          <IonSelectOption value="65">NORTH BRANCH GLOBEMED                   </IonSelectOption>
                          <IonSelectOption value="66">NORTH BRANCH Mednet Bahrain             </IonSelectOption>
                          <IonSelectOption value="67">NG MEDDY                                </IonSelectOption>
                          <IonSelectOption value="68">MINISTRY OF MUNICIPALITY AND ENVIRONMENT</IonSelectOption>
                          <IonSelectOption value="69">NORTH BRANCH MOBILITY SAINT HONORE (MSH)</IonSelectOption>
                          <IonSelectOption value="70">NORTH BRANCH MUNTAJAT                   </IonSelectOption>
                          <IonSelectOption value="71">NORTH BRANCH NAS                        </IonSelectOption>
                          <IonSelectOption value="72">NORTH BRANCH NESTLE                     </IonSelectOption>
                          <IonSelectOption value="73">NORTH BRANCH NEURON                     </IonSelectOption>
                          <IonSelectOption value="74">NORTH BRANCH NEXTCARE                   </IonSelectOption>
                          <IonSelectOption value="75">NORTH BRANCH NATIONAL HEALTH INSURANCE C</IonSelectOption>
                          <IonSelectOption value="76">NORTH BRANCH NOW HEALTH INTERNATIONAL   </IonSelectOption>
                          <IonSelectOption value="77">NORTH BRANCH OMAN INSURANCE COMPANY     </IonSelectOption>
                          <IonSelectOption value="78">NORTH BRANCH QATAR FINANCIAL CENTRE     </IonSelectOption>
                          <IonSelectOption value="79">NORTH BRANCH QIC-ANAYA                  </IonSelectOption>
                          <IonSelectOption value="80">NORTH BRANCH QLM (Q LIFE & MEDICAL INSURANCE)</IonSelectOption>
                          <IonSelectOption value="81">QATAR TRIATHLON FEDERATION ( North Gate)</IonSelectOption>
                          <IonSelectOption value="82">NORTH BRANCH SAUDI ARABIAN INSURANCE COM</IonSelectOption>
                          <IonSelectOption value="83">SUPREME COMMITTEE OF JUSTICE (North Gate)</IonSelectOption>
                          <IonSelectOption value="84">NORTH BRANCH INTERNATIONAL SOS           </IonSelectOption>
                          <IonSelectOption value="85">NORTH BRANCH TASWEEQ                     </IonSelectOption>
                          <IonSelectOption value="86">TESTAAHEL 2020 (North Gate)              </IonSelectOption>
                          <IonSelectOption value="87">NORTH BRANCH MEDNET UAE (GOLD NETWORK ON </IonSelectOption>
                          <IonSelectOption value="88">NORTH BRANCH WAPMED                      </IonSelectOption>
                          <IonSelectOption value="89">NATIONAL HEALTH INSURANCE COMPANY (NHIC) </IonSelectOption>
                          <IonSelectOption value="90">NOW HEALTH INTERNATIONAL                 </IonSelectOption>
                          <IonSelectOption value="91">OMAN INSURANCE COMPANY                   </IonSelectOption>
                          <IonSelectOption value="92">QATAR FINANCIAL CENTRE                   </IonSelectOption>
                          <IonSelectOption value="93">QIC-ANAYA                                </IonSelectOption>
                          <IonSelectOption value="94">QLM (Q LIFE & MEDICAL INSURANCE COMPANY) </IonSelectOption>
                          <IonSelectOption value="95">BEEMA-QLM (Q LIFE & MEDICAL INSURANCE CO </IonSelectOption>
                          <IonSelectOption value="96">QATAR MUSEUM AUTHORITY                   </IonSelectOption>
                          <IonSelectOption value="97">QATAR MUSEUM AUTHORITY ( North Gate )    </IonSelectOption>
                          <IonSelectOption value="98">QATAR TRIATHLON FEDERATION               </IonSelectOption>
                          <IonSelectOption value="99">QATAR TABLE TENNIS ASSOCIATION -NG       </IonSelectOption>
                          <IonSelectOption value="100">SAUDI ARABIAN INSURANCE COMPANY (SAICO) </IonSelectOption>
                          <IonSelectOption value="101">SUPREME COMMITTEE OF JUSTICE            </IonSelectOption>
                          <IonSelectOption value="102">SHEIKH JASSIM PACKAGE                   </IonSelectOption>
                          <IonSelectOption value="103">SHEIKH JASSIM KHALIFA AL THANI - PKG NG </IonSelectOption>
                          <IonSelectOption value="104">INTERNATIONAL SOS                       </IonSelectOption>
                          <IonSelectOption value="105">TASWEEQ                                 </IonSelectOption>
                          <IonSelectOption value="106">TESTAAHEL 2020 (MINISTRY OF DEFENCE)    </IonSelectOption>
                          <IonSelectOption value="107">MEDNET UAE (GOLD NETWORK ONLY)          </IonSelectOption>
                          <IonSelectOption value="108">WAPMED                                  </IonSelectOption>
                         </IonSelect>
                         </IonCol>
                        <br/>
                       
                      <IonButton fill="solid" onClick={takePhotoInsurance} size="small" class="dropdownbtn">
                          <IonIcon icon={camera} slot="start"/>
                          </IonButton>
                          <div>
                        {!takenPhotoInsurance && <h3> No photo chosen</h3>}
                        {takenPhotoInsurance && <h4 className="file">{takenPhotoInsurance.filename}</h4>}
                        </div>
                      </IonRow>
                        </div>
                    <IonRow>
                    <IonCol>
                      <IonButton
                        expand="block"
                        routerLink="/RegistrationStep4"
                        class="nextBtn"
                        strong={true}
                        >
                        NEXT
                      </IonButton>
                    </IonCol>
                  </IonRow>
                </IonCardContent>
              </IonCard>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default RegistrationStep3;
