import {
  IonButton,
  IonCard,
  IonCardContent,
  IonCol,
  IonContent,
  IonGrid,
  IonImg,
  IonPage,
  IonRow,
  IonText,
} from "@ionic/react";
import React from "react";
import Footer from "./Footer";
import Header from "./Header";
import Doctor from "../images/Doctor-01.png";
import YoutubePlay from "../images/YoutubePlay.png";
import "./BookAnAppointmentStep3.css";

const BookAnAppointmentStep3: React.FC = () => {
  interface Idetails {
    name: string;
    department: string;
    type: string;
    lastVist: string;
  }

  const arrayOfDetails: Array<Idetails> = [
    {
      name: "DR. AHMED GAAFAR ZABADY",
      department: "INTERNAL MEDICINE",
      lastVist: "26 August 2021, 1:30 PM",
      type: "INTERNAL MEDICINE CONSULTANT",
    },
  ];
  return (
    <IonPage>
      <Header title={"Book Appointment Step 3"} defaultRoute={"/Main"} />
      <IonContent>
        {arrayOfDetails.map((item) => (
          /* IMP NOTE: key should be unique, for temporary a dummy data has been assigned else look for error in console*/
          <IonCard key={item.name} class="BcardStyle">
            <IonCardContent class="BcardContentStyle">
              <IonGrid>
                <IonRow class="BfirstRow">
                  <IonCol size="2">
                    <IonImg src={Doctor}></IonImg>
                  </IonCol>
                  <IonCol size="8" class="BtextCol">
                    <IonText class="Btext1">
                      <b>{item.name}</b>
                    </IonText>
                    <IonText class="Btext2">{item.department}</IonText>
                    <IonText class="Btext3">{item.type}</IonText>
                  </IonCol>
                  <IonCol size="2" class="youtubePlay">
                    <IonImg src={YoutubePlay}></IonImg>
                  </IonCol>
                </IonRow>
                <IonRow class="BsecondRow">
                  <IonCol>
                    <IonText>
                      <b>APPOINTMENT DATE & TIME</b>
                    </IonText>
                  </IonCol>
                </IonRow>
                <IonRow class="BthirdRow">
                  <IonCol>
                    <IonText>
                      <b>{item.lastVist}</b>
                    </IonText>
                  </IonCol>
                </IonRow>
                <IonRow>
                  <IonCol class="editCol">
                    <IonButton 
                    size="small"
                    expand="full" 
                    shape="round"
                    routerLink="/BookAnAppointmentStep2"
                    >
                      <b>EDIT</b>
                    </IonButton>
                  </IonCol>
                  <IonCol class="cancelCol">
                    <IonButton
                      size="small"
                      expand="full"
                      shape="round"
                      routerLink="/Main"
                    >
                      <b>CANCEL</b>
                    </IonButton>
                  </IonCol>
                  <IonCol class="cancelCol">
                    <IonButton
                      size="small"
                      expand="full"
                      shape="round"
                      routerLink="/Main"
                    >
                      <b>Done</b>
                    </IonButton>
                  </IonCol>
                </IonRow>
              </IonGrid>
            </IonCardContent>
          </IonCard>
        ))}
      </IonContent>
      <Footer />
    </IonPage>
  );
};

export default BookAnAppointmentStep3;
