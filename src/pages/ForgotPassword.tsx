import { IonContent, IonPage } from "@ionic/react";
import React from "react";
import Footer from "./Footer";
import Header from "./Header";

const ForgotPassword: React.FC = () => {
  return (
    <IonPage>
      <Header title={"Forgot password page"} defaultRoute={"/"} />
      <IonContent></IonContent>
      <Footer />
    </IonPage>
  );
};

export default ForgotPassword;
