import React from "react";
import {
  IonCol,
  IonContent,
  IonGrid,
  IonImg,
  IonPage,
  IonRow,
} from "@ionic/react";
import "./Main.css";
import barcode from "../images/barCode.png";
import HomePageIcon1 from "../images/HomePageIcones-01.png";
import HomePageIcon2 from "../images/HomePageIcones-02.png";
import HomePageIcon3 from "../images/HomePageIcones-03.png";
import HomePageIcon4 from "../images/HomePageIcones-04.png";
import HomePageIcon5 from "../images/HomePageIcones-05.png";
import HomePageIcon6 from "../images/HomePageIcones-06.png";
import HomePageIcon7 from "../images/HomePageIcones-07.png";
import HomePageIcon8 from "../images/HomePageIcones-08.png";
import Footer from "./Footer";
import Header from "./Header";

const Main: React.FC = () => {
  interface Idetails {
    icon: any;
    url: string;
  }
  const arrayOfMainOPtions: Array<Idetails> = [
    {
      icon: HomePageIcon1,
      url: "/FindDoctor",
    },
    {
      icon: HomePageIcon2,
      url: "/BookAnAppointmentStep1",
    },
    {
      icon: HomePageIcon3,
      url: "/Prescription",
    },
    {
      icon: HomePageIcon4,
      url: "/YourDoctors",
    },
    {
      icon: HomePageIcon5,
      url: "",
    },
    {
      icon: HomePageIcon6,
      url: "",
    },
    {
      icon: HomePageIcon7,
      url: "/InsuranceApproval",
    },
    {
      icon: HomePageIcon8,
      url: "",
    },
  ];
  return (
    <IonPage>
      <Header title={"Welcome to Al Emadi Hospital"} defaultRoute={"/"} />
      <IonContent>
        <IonGrid class="grid">
          <IonRow>
            <IonCol class="details" size="3">
              <b> Name</b>
            </IonCol>
            <IonCol class="details" size="1">
              <b>:</b>
            </IonCol>
            <IonCol class="details" size="8">
              <b>Arshad</b>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol class="details" size="3">
              <b>File ID</b>
            </IonCol>
            <IonCol class="details" size="1">
              <b> :</b>
            </IonCol>
            <IonCol class="details" size="8">
              <b>27018</b>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol class="details" size="3">
              <b>Qatar ID</b>
            </IonCol>
            <IonCol class="details" size="1">
              <b>:</b>
            </IonCol>
            <IonCol class="details" size="8">
              <b>28535659269</b>
            </IonCol>
          </IonRow>
          <IonRow class="divider">
            <IonCol class="colBarcode">
              <IonImg class="barcode" src={barcode} />
            </IonCol>
          </IonRow>
          <IonRow>
            {arrayOfMainOPtions.map((items) => (
              /* IMP NOTE: key should be unique, for temporary a dummy data has been assigned else look for error in console*/
              <IonCol key={items.icon} size="4" class="colContent">
                <a href={items.url}>
                  <img src={items.icon} alt="icon missing"></img>
                </a>
              </IonCol>
            ))}
          </IonRow>
        </IonGrid>
      </IonContent>
      <Footer />
    </IonPage>
  );
};

export default Main;
