import {
  IonButton,
  IonCol,
  IonContent,
  IonGrid,
  IonImg,
  IonPage,
  IonRow,
  IonSelect,
  IonSelectOption,
  IonText,
} from "@ionic/react";
import React, { useEffect, useState } from "react";
import Footer from "./Footer";
import Header from "./Header";
import "./BookAnAppointmentStep1.css";
import Doctor from "../images/Doctor-01.png";
import axios from "axios";

const options = {
  cssClass: "my-custom-interface",
};

const BookAnAppointmentStep1: React.FC = () => {
  var temp_offerings: string[] = [];
  let arrayOfDetails: Idetails[] = [];
  const intial_index = 0;
  const [offerings, setOfferings] = useState<string[]>([]);
  const [details, setdetails] = useState<Idetails[]>([]);
  const [value, setValue] = useState<string>();

  /* : Use to improvise the code
  interface IDepartment {
    name: string;
    doctorDetails: Array<IDoctorDetails>;
  }
  interface IDoctorDetails {
    name: string;
    CliniCode: number;
  }
  
  const arrayOfDetails: Array<IDepartment> = [];
  const docDtls: Array<IDoctorDetails> = [];
  const PopulateDepartment = (data: any) => {
    arrayOfDetails = [];
    data.map((d: any) => {
      docDtls = [];
      d.doctors.map((doc: any) => {
        docDtls.push({ name: doc.name, CliniCode: doc.CliniCode });
      });
      arrayOfDetails.push({ name: d.name, doctorDetails: docDtls });
    });
    console.log(arrayOfDetails);
  };
  
  const GetDoctorList = () => {
    axios
      .get<any>("http://34.131.82.122:3001/alemadi/mobile/doctor/list")
      .then((response) => {
        console.log(response.data.departments);
        setdepartmentDetails(response.data.departments);
        PopulateDepartment(response.data.departments);
      });
  };
  
  useEffect(() => {
    GetDoctorList();
  }, []);
  */

  interface Idetails {
    name: string;
    department: string;
    cliniccode: number;
  }

  const PopulateDepartment = (data: any) => {
    arrayOfDetails = [];
    temp_offerings = [];
    data.forEach((d: any) => {
      temp_offerings.push(d.name);
      d.doctors.forEach((doc: any) => {
        arrayOfDetails.push({
          name: doc.name,
          department: d.name,
          cliniccode: doc.clinicCode,
        });
      });
    });
    setOfferings(temp_offerings);
    setdetails(arrayOfDetails);
    setValue(temp_offerings[intial_index]);
  };

  const GetDoctorList = () => {
    axios
      .get<any>("http://34.131.82.122:3001/alemadi/mobile/doctor/list")
      .then((response) => {
        PopulateDepartment(response.data.departments);
      });
  };

  useEffect(() => {
    GetDoctorList();
  }, []);

  const updateIndex = (e: any) => {
    setValue(e.detail.value);
  };

  return (
    <IonPage>
      <Header title={"Book Appointment"} defaultRoute={"/Main"} />
      <IonContent>
        <IonGrid class="b1-grid">
          <IonRow class="b1-selectRow">
            <IonCol>
              <IonSelect
                interfaceOptions={options}
                class="b1-select"
                value={value}
                onIonChange={updateIndex}
              >
                {offerings.map((offerings, i) => (
                  <IonSelectOption value={offerings} key={i}>
                    {offerings}
                  </IonSelectOption>
                ))}{" "}
              </IonSelect>
            </IonCol>
          </IonRow>
          {details
            .filter((i) => i.department === value)
            .map((item, i) => (
              <IonRow class="b1-row" key={i}>
                <IonCol size="2">
                  <IonImg src={Doctor}></IonImg>
                </IonCol>
                <IonCol size="6" class="b1-textCol">
                  <IonText class="b1-text1">
                    <b>{item.name}</b>
                  </IonText>
                  <IonText class="b1-text2">{item.department}</IonText>
                </IonCol>
                <IonCol size="4">
                  <IonButton
                    size="small"
                    class="b1-btn"
                    routerLink="/BookAnAppointmentStep2"
                  >
                    BOOK APPOINTMENT
                  </IonButton>
                </IonCol>
              </IonRow>
            ))}
        </IonGrid>
      </IonContent>
      <Footer />
    </IonPage>
  );
};

export default BookAnAppointmentStep1;
