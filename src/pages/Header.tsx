import {
  IonBackButton,
  IonButtons,
  IonHeader,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import React from "react";
import "./Header.css";

const Header: React.FC<{ title: string; defaultRoute: string }> = (props) => {
  return (
    <IonHeader>
      <IonToolbar class="ionToolbar">
        <IonButtons slot="start">
          <IonBackButton defaultHref={props.defaultRoute} />
        </IonButtons>
        <IonTitle>{props.title}</IonTitle>
      </IonToolbar>
    </IonHeader>
  );
};

export default Header;
