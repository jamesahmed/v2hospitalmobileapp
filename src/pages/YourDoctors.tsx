import {
  IonButton,
  IonCard,
  IonCardContent,
  IonCol,
  IonContent,
  IonGrid,
  IonImg,
  IonPage,
  IonRow,
  IonText,
} from "@ionic/react";
import React from "react";
import Footer from "./Footer";
import Header from "./Header";
import "./YourDoctors.css";
import Doctor from "../images/Doctor-01.png";

const YourDoctors: React.FC = () => {
  interface Idetails {
    name: string;
    department: string;
    type: string;
    lastVist: string;
  }

  const arrayOfDetails: Array<Idetails> = [
    {
      name: "DR. ABDULHAK SADALLA1",
      department: "INTERNAL MEDICINE",
      lastVist: "15 April 2021, 11:15 AM",
      type: "INTERNAL MEDICINE CONSULTANT",
    },
    {
      name: "DR. ABDULHAK SADALLA2",
      department: "INTERNAL MEDICINE",
      lastVist: "15 April 2021, 11:15 AM",
      type: "INTERNAL MEDICINE CONSULTANT",
    },
    {
      name: "DR. ABDULHAK SADALLA3",
      department: "INTERNAL MEDICINE",
      lastVist: "15 April 2021, 11:15 AM",
      type: "INTERNAL MEDICINE CONSULTANT",
    },
  ];
  return (
    <IonPage>
      <Header title={"YourDoctors page"} defaultRoute={"/Main"} />
      <IonContent>
        {arrayOfDetails.map((item) => (
          /* IMP NOTE: key should be unique, for temporary a dummy data has been assigned else look for error in console*/
          <IonCard key={item.name} class="cardStyle">
            <IonCardContent class="cardContentStyle">
              <IonGrid>
                <IonRow class="firstRow">
                  <IonCol size="2">
                    <IonImg src={Doctor}></IonImg>
                  </IonCol>
                  <IonCol size="10" class="textCol">
                    <IonText class="text1">
                      <b>{item.name}</b>
                    </IonText>
                    <IonText class="text2">{item.department}</IonText>
                    <IonText class="text3">{item.type}</IonText>
                  </IonCol>
                </IonRow>
                <IonRow class="secondRow">
                  <IonCol>
                    <IonText>
                      <b>LAST VISITE</b>
                    </IonText>
                  </IonCol>
                </IonRow>
                <IonRow class="thirdRow">
                  <IonCol>
                    <IonText>
                      <b>{item.lastVist}</b>
                    </IonText>
                  </IonCol>
                </IonRow>
                <IonRow>
                  <IonCol>
                    <IonButton size="small">DIAGNOSIS</IonButton>
                    <IonButton size="small">ORDERS</IonButton>
                    <IonButton size="small">PRESCRIPTION</IonButton>
                  </IonCol>
                </IonRow>
              </IonGrid>
            </IonCardContent>
          </IonCard>
        ))}
      </IonContent>
      <Footer />
    </IonPage>
  );
};

export default YourDoctors;
